﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLayer.Enums
{
    public enum UserGameState
    {
        NOT_IN_GAME, IN_LOBBY, IN_GAME
    }
}
