﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLayer.Models
{
    public class GameHistoryToView
    {
        public int GameId { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public string GameOwner { get; set; }
        public string Player2 { get; set; }
        public string Winner { get; set; }
        public List<GameMoveModel> GameMoves { get; set; }
    }
}
