﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLayer.Models
{
    public class GameMoveModel
    {
        public int GameId { get; set; }
        public int PosX { get; set; }
        public int PosY { get; set; }
        public string OwnerName { get; set; }
        public DateTime TimeStamp { get; set; }
    }
}
