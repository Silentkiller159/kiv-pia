﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLayer.Models
{
    public class GameMoveViewModel
    {
        public int PosX { get; set; }
        public int PosY { get; set; }
        public string ImgPath { get; set; }
        public DateTime TimeStamp { get; set; }
    }
}
