﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLayer.Models
{
    public class GameSizeModel
    {
        public int Width { get; set; }
        public int Height { get; set; }
    }
}
