﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLayer.Models
{
    public class GameToViewInfo
    {
        public int Id { get; set; }
        public string Result { get; set; }
        public List<string> oponents { get; set; }
    }
}
