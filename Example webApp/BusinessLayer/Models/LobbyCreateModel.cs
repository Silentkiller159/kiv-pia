﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLayer.Models
{
    public class LobbyCreateModel
    {
        public string BoardWidth { get; set; }
        public string BoardHeight { get; set; }
    }
}
