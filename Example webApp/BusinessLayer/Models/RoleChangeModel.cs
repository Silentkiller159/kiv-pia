﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLayer.Models
{
    public class RoleChangeModel
    {
        public string UserName { get; set; }
        public string NewRole { get; set; }
    }
}
