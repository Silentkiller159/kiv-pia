﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLayer.Models
{
    public class UserToFriendModel
    {
        public string UserName { get; set; }
        public string FriendName { get; set; }
        public bool Accepted { get; set; }
        public bool FriendOnline { get; set; }
        public bool FriendInGame { get; set; }
        public bool FriendInLobby { get; set; }
    }
}
