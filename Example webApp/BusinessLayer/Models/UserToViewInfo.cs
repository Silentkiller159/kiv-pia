﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLayer.Models
{
    public class UserToViewInfo
    {
        public string UserName { get; set; }
        public string Role { get; set; }
        public bool InLobby { get; set; }
        public bool InGame { get; set; }

    }
}
