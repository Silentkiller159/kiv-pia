﻿using BusinessLayer.Enums;
using BusinessLayer.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLayer.Repositories
{
    public interface IGameRepository
    {
        public UserGameState GetUserGameState(string userId);
        public string GetGameOwnerId(int gameId);


        #region Lobby Actions
        public int CreateGameLobby(string ownerId, int width, int height);

        public int GetUserActiveGameLobbyId(string userId);
        public bool IsUserLobbyOwner(string userId, int lobbyId);
        public bool IsUserInvitedToLobby(string userId, int lobbyId);
        public bool JoinLobby(string userId, int lobbyId);
        public bool DeclineGameRequest(string userId, int lobbyId);
        public bool SetLobbyOwner(string userId, int lobbyId);

        public bool InviteUserToLobby(string userId, int lobbyId);

        public List<int> GetAllUsersInvites(string userId);

        public List<string> GetAllPlayersOfGameIds(int gameId, bool lobbyState);
        public List<UserToViewInfo> GetAllLobbyInvites(int lobbyId);

        public bool LeaveGameLobby(string userId, int lobbyId);
        public bool CleanLobbyInvites(int lobbyId);
        public bool RemoveGameLobby(int lobbyId);

        #endregion

        #region Game Actions
        public bool StartGame(int gameId);

        public GameSizeModel GetGameSize(int gameId);

        public bool AcceptGameMove(int gameId, string userId, int x, int y);
        public bool IsBoardPositionEmpty(int gameId, int x, int y);
        public string GetCurrentPlayer(int gameId);
        public bool ChangeCurrentPlayer(int gameId, string userId);

        public bool EndGame(int gameId, string winnerId);

        public List<GameMoveModel> GetGameBoardHistory(int gameId);

        public int GetUserActiveGameId(string userId);

        public List<string> GetGameOponents(string userId, int gameId);

        public string GetGameWinner(int gameId);

        public List<GameToViewInfo> GetAllFinishedGamesForUser(string userId);

        #endregion



    }
}
