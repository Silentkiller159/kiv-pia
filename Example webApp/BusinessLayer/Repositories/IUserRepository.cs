﻿using BusinessLayer.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLayer.Repositories
{
    public interface IUserRepository
    {
        public string GetUserIdByName(string userName);

        public string GetUserNameById(string userId);

        public List<UserToViewInfo> GetAllUsers();

        public bool HasFriendRelation(string fromUserId, string toUserId);

        public int SendFriendRequest(string fromUserId, string toUserId);

        public int ProcessFriendRequest(string fromUserId, string toUserId, bool accepted);

        public int RemoveFriend(string fromUserId, string toUserId);

        public IEnumerable<UserToFriendModel> GetAllFriends(string userId);

        public bool ChangeUserPassword(string userId, string newPass);
        public bool ChangeUserPasswordValidate(string userId, string newPass, string oldPass);
        public bool ChangeUserRole(string userId, string newRole);
    }
}
