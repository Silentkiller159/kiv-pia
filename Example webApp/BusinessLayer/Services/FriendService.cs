﻿using BusinessLayer.Models;
using BusinessLayer.Repositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLayer.Services
{
    public class FriendService : IFriendService
    {
        private IUserRepository _userRepo;

        public FriendService(IUserRepository userRepo)
        {
            _userRepo = userRepo;
        }

        public IEnumerable<UserToFriendModel> GetAllFriends(string userId)
        {
            return _userRepo.GetAllFriends(userId);
        }

        public int ProcessFriendrequest(string fromUserId, string toUserId, bool accepted)
        {
            return _userRepo.ProcessFriendRequest(fromUserId, toUserId, accepted);
        }

        public int RemoveFriend(string meUserId, string friendUserId)
        {
            return _userRepo.RemoveFriend(meUserId, friendUserId);
        }

        public int SendFriendRequest(string fromUserId, string toUserId)
        {
            if(_userRepo.HasFriendRelation(fromUserId, toUserId))
            {
                return -1;
            }
            else
            {
                return _userRepo.SendFriendRequest(fromUserId, toUserId);
            }
        }
    }
}
