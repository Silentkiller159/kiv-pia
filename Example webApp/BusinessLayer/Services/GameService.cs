﻿using BusinessLayer.Enums;
using BusinessLayer.Models;
using BusinessLayer.Repositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLayer.Services
{
    public class GameService : IGameService
    {
        private IGameRepository _gameRepo;

        public GameService(IGameRepository gameRepo)
        {
            _gameRepo = gameRepo;
        }

        public int CreateGameLobby(string userId, int width, int heigth)
        {
            if (_gameRepo.GetUserActiveGameLobbyId(userId) < 0 && _gameRepo.GetUserActiveGameId(userId) < 0)
            {
                return _gameRepo.CreateGameLobby(userId, width, heigth);
            }
            return -1;
        }

        public bool LeaveGameLobby(string userId)
        {
            int lobbyId = _gameRepo.GetUserActiveGameLobbyId(userId);

            if(lobbyId > -1)
            {
                var playerList = _gameRepo.GetAllPlayersOfGameIds(lobbyId, true);
                var newOwnerId = "";
                foreach(string playerId in playerList)
                {
                    if(playerId != userId)
                    {
                        newOwnerId = playerId;
                        break;
                    }
                }

                if(playerList.Count > 0)
                {
                    if(playerList.Count == 1)
                    {
                        return _gameRepo.RemoveGameLobby(lobbyId);
                    }
                    else
                    {
                        if (_gameRepo.IsUserLobbyOwner(userId, lobbyId))
                        {
                            _gameRepo.SetLobbyOwner(newOwnerId, lobbyId);
                        }
                        return _gameRepo.LeaveGameLobby(userId, lobbyId);
                    }
                }
                return false;
            }

            return false;
        }

        public List<string> GetAllPlayersOfLobbyIds(int lobbyId)
        {
            return _gameRepo.GetAllPlayersOfGameIds(lobbyId, true);
        }

        public List<GameMoveModel> GetGameHistory(int gameId)
        {
            return _gameRepo.GetGameBoardHistory(gameId);
        }

        public UserGameState GetUserGameState(string userId)
        {
            return _gameRepo.GetUserGameState(userId);
        }

        public int InviteToGameLobby(string invitedUserId, string myId)
        {
            int lobbyId = _gameRepo.GetUserActiveGameLobbyId(myId);

            if (lobbyId > -1)
            {
                if (_gameRepo.GetUserActiveGameLobbyId(invitedUserId) < 0 && _gameRepo.GetUserActiveGameId(invitedUserId) < 0)
                {
                    if(!_gameRepo.IsUserInvitedToLobby(invitedUserId, lobbyId))
                    {
                        _gameRepo.InviteUserToLobby(invitedUserId, lobbyId);
                        return lobbyId;
                    }
                }          
            }
            return -1;
        }

        public List<int> GetUserInvites(string userId)
        {
            return _gameRepo.GetAllUsersInvites(userId);
        }
        public List<UserToViewInfo> GetGameInvites(int gameId)
        {
            return _gameRepo.GetAllLobbyInvites(gameId);
        }


        public bool ProcessGameInviteRequest(string myId, int lobbyId, bool join)
        {
            if(_gameRepo.IsUserInvitedToLobby(myId, lobbyId))
            {
                if (join)
                {
                    if(_gameRepo.GetAllPlayersOfGameIds(lobbyId, true).Count < 2)
                    {
                        return _gameRepo.JoinLobby(myId, lobbyId);
                    }
                }
                else
                {
                    return _gameRepo.DeclineGameRequest(myId, lobbyId);
                }
            }
            return false;
        }

        public bool StartCurrentGame(string userId)
        {
            int lobbyId = _gameRepo.GetUserActiveGameLobbyId(userId);
            if(lobbyId < 0)
            {
                return false;
            }
            if (_gameRepo.IsUserLobbyOwner(userId, lobbyId))
            {
                if (_gameRepo.GetAllPlayersOfGameIds(lobbyId, true).Count > 1)
                {
                    //More than 1 palyer, can start game
                    _gameRepo.CleanLobbyInvites(lobbyId);

                    return _gameRepo.StartGame(lobbyId);
                }
            }
            return false;
        }
        public GameSizeModel GetCurrentGameSize(string userId)
        {
            int gameId = _gameRepo.GetUserActiveGameId(userId);
            if (gameId > -1)
            {
                return _gameRepo.GetGameSize(gameId);
            }

            return null;
        }

        public List<string> GetAllPlayersOfGameIds(int gameId)
        {
            return _gameRepo.GetAllPlayersOfGameIds(gameId, false);
        }

        public bool AcceptGameMove(string userId, int x, int y)
        {
            int gameId = GetUserActiveGameId(userId);
            if (gameId > -1)
            {
                string currentPlayer = _gameRepo.GetCurrentPlayer(gameId);
                if (currentPlayer == userId)
                {
                    if (_gameRepo.IsBoardPositionEmpty(gameId, x, y))
                    {
                        if(_gameRepo.AcceptGameMove(gameId, userId, x, y))
                        {
                            return changePlayer(userId, gameId);
                        }
                    }
                }
            }
            return false;
        }

        public string GetCurrentGamePlayerId(int gameId)
        {
            return _gameRepo.GetCurrentPlayer(gameId);
        }

        public string GetCurrentGameOwnerId(int gameId)
        {
            return _gameRepo.GetGameOwnerId(gameId);
        }
        private bool changePlayer(string currentPlayer, int gameId)
        {
            string newPlayerId;
            List<string> players = GetAllPlayersOfGameIds(gameId);

            if(players.Count > 1)
            {
                newPlayerId = players[0];

                bool match = false;
                foreach(var player in players)
                {
                    if (match)
                    {
                        newPlayerId = player;
                        break;
                    }
                    if(player == currentPlayer)
                    {
                        match = true;
                    }
                }

                return _gameRepo.ChangeCurrentPlayer(gameId, newPlayerId);
            }

            return false;
        }

        public int GetUserActiveGameLobbyId(string userId)
        {
            return _gameRepo.GetUserActiveGameLobbyId(userId);
        }

        public int GetUserActiveGameId(string userId)
        {
            return _gameRepo.GetUserActiveGameId(userId);
        }

        public bool EndGame(int gameId, string winnerId)
        {
            return _gameRepo.EndGame(gameId, winnerId);
        }

        public List<GameToViewInfo> GetAllFinishedGamesForUser(string userId)
        {
            return _gameRepo.GetAllFinishedGamesForUser(userId);
        }

        public GameHistoryToView GetFinishedGameHistory(int gameId)
        {
            GameHistoryToView gh = new GameHistoryToView();
            gh.GameId = gameId;
            gh.GameOwner = GetCurrentGameOwnerId(gameId);
            gh.Player2 = _gameRepo.GetGameOponents(gh.GameOwner, gameId)[0];
            gh.Winner = _gameRepo.GetGameWinner(gameId);
            gh.GameMoves = GetGameHistory(gameId);
            var gameSize = _gameRepo.GetGameSize(gameId);
            if(gameSize != null)
            {
                gh.Width = gameSize.Width;
                gh.Height = gameSize.Height;
            }
            return gh;
        }
    }
}
