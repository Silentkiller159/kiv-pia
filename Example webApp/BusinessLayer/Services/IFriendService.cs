﻿using BusinessLayer.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLayer.Services
{
    public interface IFriendService
    {
        public IEnumerable<UserToFriendModel> GetAllFriends(string userId);
        public int SendFriendRequest(string fromUserId, string toUserId);
        public int ProcessFriendrequest(string fromUserId, string toUserId, bool accepted);
        public int RemoveFriend(string meUserId, string friendUserId);

    }
}
