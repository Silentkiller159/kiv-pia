﻿using BusinessLayer.Enums;
using BusinessLayer.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLayer.Services
{
    public interface IGameService
    {
        public int CreateGameLobby(string userId, int width, int heigth);
        public int InviteToGameLobby(string invitedUserId, string myId);
        public List<int> GetUserInvites(string userId);
        public List<UserToViewInfo> GetGameInvites(int gameId);

        public bool ProcessGameInviteRequest(string myId, int lobbyId, bool join);
        public bool LeaveGameLobby(string userId);

        public List<string> GetAllPlayersOfLobbyIds(int lobbyId);



        public bool StartCurrentGame(string userId);

        public GameSizeModel GetCurrentGameSize(string userId);

        public List<string> GetAllPlayersOfGameIds(int gameId);

        public string GetCurrentGameOwnerId(int gameId);

        public string GetCurrentGamePlayerId(int gameId);


        public bool AcceptGameMove(string userId, int x, int y);

        public bool EndGame(int gameId, string winnerId);

        public List<GameMoveModel> GetGameHistory(int gameId);

        public UserGameState GetUserGameState(string userId);

        public int GetUserActiveGameLobbyId(string userId);
        public int GetUserActiveGameId(string userId);

        public List<GameToViewInfo> GetAllFinishedGamesForUser(string userId);

        public GameHistoryToView GetFinishedGameHistory(int gameId);


    }
}
