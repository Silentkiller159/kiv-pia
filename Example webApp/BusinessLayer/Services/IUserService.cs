﻿using BusinessLayer.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLayer.Services
{
    public interface IUserService
    {
        public bool ChangePassword(string userId, string newPassword);
        public bool ChangePasswordValidate(string userId, string newPassword, string oldPass);
        public string GetUserIdByName(string userName);
        public string GetUserNameById(string userId);
        public List<UserToViewInfo> GetAllUsers();

        public bool ChangeUserRole(string userId, string newRole);

    }
}
