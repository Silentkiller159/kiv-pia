﻿using BusinessLayer.Models;
using BusinessLayer.Repositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLayer.Services
{
    public class UserService : IUserService
    {
        private IUserRepository _userRepo;


        public bool ChangePassword(string userId, string newPassword)
        {
            return _userRepo.ChangeUserPassword(userId, newPassword);
        }

        public bool ChangePasswordValidate(string userId, string newPassword, string oldPass)
        {
            return _userRepo.ChangeUserPasswordValidate(userId, newPassword, oldPass);
        }

        public UserService(IUserRepository userRepo)
        {
            _userRepo = userRepo;
        }

        public string GetUserIdByName(string userName)
        {
            return _userRepo.GetUserIdByName(userName);
        }

        public string GetUserNameById(string userId)
        {
            return _userRepo.GetUserNameById(userId);
        }

        public List<UserToViewInfo> GetAllUsers()
        {
            return _userRepo.GetAllUsers();
        }

        public bool ChangeUserRole(string userId, string newRole)
        {
            if(newRole == "ADMIN" || newRole == "USER")
            {
                return _userRepo.ChangeUserRole(userId, newRole);
            }

            return false;
        }
    }
}
