﻿using DataAccess.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccess
{
    public class DBContext : IdentityDbContext
    {

        public DbSet<FriendListEntity> friendList { get; set; }
        public DbSet<UserToGameEntity> userToGame { get; set; }
        public DbSet<GameEntity> games { get; set; }
        public DbSet<GameMovesEntity> gameMoves { get; set; }

        public DBContext(DbContextOptions<DBContext> opt) : base(opt)
        {

        }
    }
}
