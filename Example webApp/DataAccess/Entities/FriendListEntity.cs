﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DataAccess.Entities
{
    /// <summary>
    /// FriendList for a player
    /// If not currently accepted, accepted = false
    /// </summary>
    public class FriendListEntity
    {
        public int Id { get; set; }
        public string UserID { get; set; }

        [ForeignKey(nameof(UserID))]
        public virtual IdentityUser User { get; set; }
        public string FriendID { get; set; }

        [ForeignKey(nameof(FriendID))]
        public virtual IdentityUser Friend { get; set; }
        public bool Accepted { get; set; }
    }
}
