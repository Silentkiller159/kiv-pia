﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DataAccess.Entities
{

    /// <summary>
    /// Table for one game
    /// </summary>
    public class GameEntity
    {
        public int Id { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public string OwnerID { get; set; }

        [ForeignKey(nameof(OwnerID))]
        public virtual IdentityUser Owner { get; set; }

        public bool Running { get; set; }
        public bool LobbyState { get; set; }
        public string CurrentPlayerID { get; set; }

        [ForeignKey(nameof(CurrentPlayerID))]
        public virtual IdentityUser CurrentPlayer { get; set; }
        public string WinnerID { get; set; }

        [ForeignKey(nameof(WinnerID))]
        public virtual IdentityUser Winner { get; set; }


    }
}
