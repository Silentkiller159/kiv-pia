﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DataAccess.Entities
{
    /// <summary>
    /// Table of game moves.
    /// Each move is stored, to be available to show replay
    /// </summary>
    public class GameMovesEntity
    {
        public int Id { get; set; }
        public int GameId { get; set; }

        [ForeignKey(nameof(GameId))]
        public virtual GameEntity Game { get; set; }
        public int PosX { get; set; }
        public int PosY { get; set; }
        public string OwnerID { get; set; }

        [ForeignKey(nameof(OwnerID))]
        public virtual IdentityUser Owner { get; set; }
        public DateTime TimeStamp { get; set; }
    }
}
