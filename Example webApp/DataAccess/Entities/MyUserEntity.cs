﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccess.Entities
{
    public class MyUserEntity : IdentityUser
    {
        public DateTime LastActive { get; set;}
    }
}
