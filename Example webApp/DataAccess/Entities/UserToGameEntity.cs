﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DataAccess.Entities
{
    public class UserToGameEntity
    {
        public int Id { get; set; }
        public string UserID { get; set; }

        [ForeignKey(nameof(UserID))]
        public virtual IdentityUser User { get; set; }
        public int GameID { get; set; }

        [ForeignKey(nameof(GameID))]
        public virtual GameEntity Game { get; set; }
        public bool Active { get; set; }
        public bool AcceptedRequest { get; set; }
    }
}
