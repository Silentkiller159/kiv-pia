﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DataAccess.Migrations
{
    public partial class AddedTablesControl : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CurrentPlayer",
                table: "games");

            migrationBuilder.DropColumn(
                name: "Status",
                table: "games");

            migrationBuilder.DropColumn(
                name: "Winner",
                table: "games");

            migrationBuilder.AddColumn<int>(
                name: "CurrentPlayerID",
                table: "games",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<bool>(
                name: "Running",
                table: "games",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<int>(
                name: "WinnerID",
                table: "games",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "friendList",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    UserID = table.Column<int>(nullable: false),
                    FriendID = table.Column<int>(nullable: false),
                    Accepted = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_friendList", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "gameMoves",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    GameId = table.Column<int>(nullable: false),
                    PosX = table.Column<int>(nullable: false),
                    PosY = table.Column<int>(nullable: false),
                    OwnerID = table.Column<int>(nullable: false),
                    TimeStamp = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_gameMoves", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "userToGame",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    UserID = table.Column<int>(nullable: false),
                    GameID = table.Column<int>(nullable: false),
                    Active = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_userToGame", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "friendList");

            migrationBuilder.DropTable(
                name: "gameMoves");

            migrationBuilder.DropTable(
                name: "userToGame");

            migrationBuilder.DropColumn(
                name: "CurrentPlayerID",
                table: "games");

            migrationBuilder.DropColumn(
                name: "Running",
                table: "games");

            migrationBuilder.DropColumn(
                name: "WinnerID",
                table: "games");

            migrationBuilder.AddColumn<string>(
                name: "CurrentPlayer",
                table: "games",
                type: "longtext CHARACTER SET utf8mb4",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Status",
                table: "games",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "Winner",
                table: "games",
                type: "longtext CHARACTER SET utf8mb4",
                nullable: true);
        }
    }
}
