﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DataAccess.Migrations
{
    public partial class ForeignKeys : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "UserID",
                table: "userToGame",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AlterColumn<string>(
                name: "WinnerID",
                table: "games",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AlterColumn<string>(
                name: "CurrentPlayerID",
                table: "games",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AlterColumn<string>(
                name: "OwnerID",
                table: "gameMoves",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AlterColumn<string>(
                name: "UserID",
                table: "friendList",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AlterColumn<string>(
                name: "FriendID",
                table: "friendList",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.CreateIndex(
                name: "IX_userToGame_GameID",
                table: "userToGame",
                column: "GameID");

            migrationBuilder.CreateIndex(
                name: "IX_userToGame_UserID",
                table: "userToGame",
                column: "UserID");

            migrationBuilder.CreateIndex(
                name: "IX_games_CurrentPlayerID",
                table: "games",
                column: "CurrentPlayerID");

            migrationBuilder.CreateIndex(
                name: "IX_games_WinnerID",
                table: "games",
                column: "WinnerID");

            migrationBuilder.CreateIndex(
                name: "IX_gameMoves_GameId",
                table: "gameMoves",
                column: "GameId");

            migrationBuilder.CreateIndex(
                name: "IX_gameMoves_OwnerID",
                table: "gameMoves",
                column: "OwnerID");

            migrationBuilder.CreateIndex(
                name: "IX_friendList_FriendID",
                table: "friendList",
                column: "FriendID");

            migrationBuilder.CreateIndex(
                name: "IX_friendList_UserID",
                table: "friendList",
                column: "UserID");

            migrationBuilder.AddForeignKey(
                name: "FK_friendList_AspNetUsers_FriendID",
                table: "friendList",
                column: "FriendID",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_friendList_AspNetUsers_UserID",
                table: "friendList",
                column: "UserID",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_gameMoves_games_GameId",
                table: "gameMoves",
                column: "GameId",
                principalTable: "games",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_gameMoves_AspNetUsers_OwnerID",
                table: "gameMoves",
                column: "OwnerID",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_games_AspNetUsers_CurrentPlayerID",
                table: "games",
                column: "CurrentPlayerID",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_games_AspNetUsers_WinnerID",
                table: "games",
                column: "WinnerID",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_userToGame_games_GameID",
                table: "userToGame",
                column: "GameID",
                principalTable: "games",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_userToGame_AspNetUsers_UserID",
                table: "userToGame",
                column: "UserID",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_friendList_AspNetUsers_FriendID",
                table: "friendList");

            migrationBuilder.DropForeignKey(
                name: "FK_friendList_AspNetUsers_UserID",
                table: "friendList");

            migrationBuilder.DropForeignKey(
                name: "FK_gameMoves_games_GameId",
                table: "gameMoves");

            migrationBuilder.DropForeignKey(
                name: "FK_gameMoves_AspNetUsers_OwnerID",
                table: "gameMoves");

            migrationBuilder.DropForeignKey(
                name: "FK_games_AspNetUsers_CurrentPlayerID",
                table: "games");

            migrationBuilder.DropForeignKey(
                name: "FK_games_AspNetUsers_WinnerID",
                table: "games");

            migrationBuilder.DropForeignKey(
                name: "FK_userToGame_games_GameID",
                table: "userToGame");

            migrationBuilder.DropForeignKey(
                name: "FK_userToGame_AspNetUsers_UserID",
                table: "userToGame");

            migrationBuilder.DropIndex(
                name: "IX_userToGame_GameID",
                table: "userToGame");

            migrationBuilder.DropIndex(
                name: "IX_userToGame_UserID",
                table: "userToGame");

            migrationBuilder.DropIndex(
                name: "IX_games_CurrentPlayerID",
                table: "games");

            migrationBuilder.DropIndex(
                name: "IX_games_WinnerID",
                table: "games");

            migrationBuilder.DropIndex(
                name: "IX_gameMoves_GameId",
                table: "gameMoves");

            migrationBuilder.DropIndex(
                name: "IX_gameMoves_OwnerID",
                table: "gameMoves");

            migrationBuilder.DropIndex(
                name: "IX_friendList_FriendID",
                table: "friendList");

            migrationBuilder.DropIndex(
                name: "IX_friendList_UserID",
                table: "friendList");

            migrationBuilder.AlterColumn<int>(
                name: "UserID",
                table: "userToGame",
                type: "int",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "WinnerID",
                table: "games",
                type: "int",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "CurrentPlayerID",
                table: "games",
                type: "int",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "OwnerID",
                table: "gameMoves",
                type: "int",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "UserID",
                table: "friendList",
                type: "int",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "FriendID",
                table: "friendList",
                type: "int",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);
        }
    }
}
