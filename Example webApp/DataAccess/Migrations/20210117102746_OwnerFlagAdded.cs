﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DataAccess.Migrations
{
    public partial class OwnerFlagAdded : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "OwnerID",
                table: "games",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_games_OwnerID",
                table: "games",
                column: "OwnerID");

            migrationBuilder.AddForeignKey(
                name: "FK_games_AspNetUsers_OwnerID",
                table: "games",
                column: "OwnerID",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_games_AspNetUsers_OwnerID",
                table: "games");

            migrationBuilder.DropIndex(
                name: "IX_games_OwnerID",
                table: "games");

            migrationBuilder.DropColumn(
                name: "OwnerID",
                table: "games");
        }
    }
}
