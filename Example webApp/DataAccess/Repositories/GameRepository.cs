﻿using BusinessLayer.Enums;
using BusinessLayer.Models;
using BusinessLayer.Repositories;
using DataAccess.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAccess.Repositories
{
    public class GameRepository : IGameRepository
    {
        private DBContext _dbContext;

        public GameRepository(DBContext context)
        {
            _dbContext = context;
        }

        #region Lobby actions
        public int CreateGameLobby(string ownerId, int width, int height)
        {
            GameEntity game = new GameEntity();
            game.OwnerID = ownerId;
            game.Width = width;
            game.Height = height;
            game.LobbyState = true;
            _dbContext.games.Add(game);
            _dbContext.SaveChanges();


            _dbContext.userToGame.Add(new UserToGameEntity() { UserID = ownerId, Active = true, AcceptedRequest = true, GameID = game.Id });
            _dbContext.SaveChanges();

            if (game.Id > -1)
            {
                return game.Id;
            }

            return -1;
        }

        public int GetUserActiveGameLobbyId(string userId)
        {
            var LobbyList = _dbContext.userToGame.Include(g => g.Game).Where(utg => utg.UserID == userId && utg.AcceptedRequest && utg.Active && utg.Game.LobbyState).ToList();
            var lobby = LobbyList.FirstOrDefault();
            if (lobby != null)
            {
                return lobby.GameID;
            }

            return -1;
        }
        public bool IsUserLobbyOwner(string userId, int lobbyId)
        {
            return _dbContext.games.Where(g => g.Id == lobbyId && g.OwnerID == userId && g.LobbyState == true).Count() > 0;
        }
        public bool SetLobbyOwner(string userId, int lobbyId)
        {
            var lobbyList = _dbContext.games.Where(g => g.Id == lobbyId && g.LobbyState == true).ToList();
            var lobby = lobbyList.FirstOrDefault();

            if (lobby != null)
            {
                lobby.OwnerID = userId;
                _dbContext.SaveChanges();
                return true;
            }
            return false;
        }
        public bool InviteUserToLobby(string userId, int lobbyId)
        {
            _dbContext.userToGame.Add(new UserToGameEntity() { UserID = userId, Active = true, AcceptedRequest = false, GameID = lobbyId });
            _dbContext.SaveChanges();

            return true;
        }
        public List<int> GetAllUsersInvites(string userId)
        {
            return _dbContext.userToGame.Include(g => g.Game).Where(utg => utg.UserID == userId && utg.AcceptedRequest == false && utg.Active && utg.Game.LobbyState).Select(utg => utg.GameID).ToList();
        }
        public bool CleanLobbyInvites(int lobbyId)
        {
            var invites = _dbContext.userToGame.Include(g => g.Game).Where(utg => utg.AcceptedRequest == false && utg.Game.LobbyState && utg.GameID == lobbyId).ToList();
            foreach(var invite in invites)
            {
                _dbContext.userToGame.Remove(invite);
            }
            _dbContext.SaveChanges();
            return true;
        }
        public bool LeaveGameLobby(string userId, int lobbyId)
        {
            var utgList = _dbContext.userToGame.Include(g => g.Game).Where(utg => utg.GameID == lobbyId && utg.UserID == userId && utg.Game.LobbyState == true).ToList();
            var utg = utgList.FirstOrDefault();
            if (utg != null)
            {
                _dbContext.userToGame.Remove(utg);
                _dbContext.SaveChanges();
                return true;
            }

            return false;
        }
        public bool RemoveGameLobby(int lobbyId)
        {
            var LobbyList = _dbContext.games.Where(g => g.Id == lobbyId && g.LobbyState == true).ToList();
            var lobby = LobbyList.FirstOrDefault();
            if (lobby != null)
            {
                _dbContext.games.Remove(lobby);
                _dbContext.SaveChanges();
                return true;
            }

            return false;
        }
        public bool IsUserInvitedToLobby(string userId, int lobbyId)
        {
            var utgList = _dbContext.userToGame.Where(utg => utg.GameID == lobbyId && utg.UserID == userId && utg.AcceptedRequest == false).ToList();
            var utg = utgList.FirstOrDefault();

            if (utg != null)
            {
                return true;
            }
            return false;
        }
        public bool JoinLobby(string userId, int lobbyId)
        {
            var utgList = _dbContext.userToGame.Where(utg => utg.GameID == lobbyId && utg.UserID == userId).ToList();
            var utg = utgList.FirstOrDefault();

            if (utg != null)
            {
                utg.AcceptedRequest = true;
                _dbContext.SaveChanges();
                return true;
            }
            return false;
        }
        public bool DeclineGameRequest(string userId, int lobbyId)
        {
            var utgList = _dbContext.userToGame.Where(utg => utg.GameID == lobbyId && utg.UserID == userId).ToList();
            var utg = utgList.FirstOrDefault();

            if (utg != null)
            {
                _dbContext.userToGame.Remove(utg);
                _dbContext.SaveChanges();
                return true;
            }
            return false;
        }
        #endregion

        #region Game actions
        public int GetUserActiveGameId(string userId)
        {
            var LobbyList = _dbContext.userToGame.Include(g => g.Game).Where(utg => utg.UserID == userId && utg.AcceptedRequest && utg.Active && !utg.Game.LobbyState && utg.Game.Running).ToList();
            var lobby = LobbyList.FirstOrDefault();
            if (lobby != null)
            {
                return lobby.GameID;
            }

            return -1;
        }
        public bool StartGame(int gameId)
        {
            var gameList = _dbContext.games.Where(g => g.Id == gameId && g.Running == false && g.LobbyState).ToList();
            var game = gameList.FirstOrDefault();

            if (game != null)
            {
                game.CurrentPlayerID = game.OwnerID;
                game.LobbyState = false;
                game.Running = true;
                _dbContext.SaveChanges();
                return true;
            }
            return false;
        }
        public GameSizeModel GetGameSize(int gameId)
        {
            GameSizeModel gsm = new GameSizeModel();
            var gameList = _dbContext.games.Where(g => g.Id == gameId).ToList();
            var game = gameList.FirstOrDefault();
            if (game != null)
            {
                gsm.Height = game.Height;
                gsm.Width = game.Width;
                return gsm;
            }
            return null;
        }
        public bool AcceptGameMove(int gameId, string userId, int x, int y)
        {
            _dbContext.gameMoves.Add(new GameMovesEntity() { GameId = gameId, PosX = x, PosY = y, OwnerID = userId, TimeStamp = DateTime.Now });
            _dbContext.SaveChanges();

            return true;
        }
        public bool IsBoardPositionEmpty(int gameId, int x, int y)
        {
            var movesList = _dbContext.gameMoves.Where(gm => gm.GameId == gameId && gm.PosX == x && gm.PosY == y).ToList();
            var move = movesList.FirstOrDefault();

            if (move != null)
            {
                return false;
            }
            return true;
        }
        public string GetCurrentPlayer(int gameId)
        {
            var gameList = _dbContext.games.Where(g => g.Id == gameId && g.Running == true && g.LobbyState == false).ToList();
            var game = gameList.FirstOrDefault();

            if (game != null)
            {
                return game.CurrentPlayerID;
            }
            return null;
        }
        public bool ChangeCurrentPlayer(int gameId, string userId)
        {
            var gameList = _dbContext.games.Where(g => g.Id == gameId && g.Running == true && g.LobbyState == false).ToList();
            var game = gameList.FirstOrDefault();

            if (game != null)
            {
                game.CurrentPlayerID = userId;
                _dbContext.SaveChanges();
                return true;
            }
            return false;
        }
        public string GetGameOwnerId(int gameId)
        {
            var gameList = _dbContext.games.Where(g => g.Id == gameId).ToList();
            var game = gameList.FirstOrDefault();

            if (game != null)
            {
                return game.OwnerID;
            }
            return null;
        }
        public bool EndGame(int gameId, string winnerId)
        {
            var gameList = _dbContext.games.Where(g => g.Id == gameId && g.Running == true && g.LobbyState == false).ToList();
            var game = gameList.FirstOrDefault();

            if (game != null)
            {
                game.Running = false;
                game.WinnerID = winnerId;
                _dbContext.SaveChanges();
                return true;
            }
            return false;
        }
        public List<GameMoveModel> GetGameBoardHistory(int gameId)
        {
            List<GameMoveModel> gmmList = new List<GameMoveModel>();
            var gameMovesList = _dbContext.gameMoves.Include(gm => gm.Owner).Where(gm => gm.GameId == gameId).OrderBy(gm => gm.Id).ToList();
            foreach (var gameMove in gameMovesList)
            {
                var gmm = new GameMoveModel();
                gmm.GameId = gameId;
                gmm.PosX = gameMove.PosX;
                gmm.PosY = gameMove.PosY;
                gmm.OwnerName = gameMove.Owner.UserName;
                gmm.TimeStamp = gameMove.TimeStamp;
                gmmList.Add(gmm);
            }
            return gmmList;
        }
        #endregion

        public UserGameState GetUserGameState(string userId)
        {
            var userGamerelationList = _dbContext.userToGame.Include(g => g.Game).Where(utg => utg.UserID == userId && utg.AcceptedRequest && (utg.Game.LobbyState || utg.Game.Running));

            var userGameRelation = userGamerelationList.FirstOrDefault();

            if(userGameRelation == null)
            {
                return UserGameState.NOT_IN_GAME;
            }

            if (userGameRelation.Game.LobbyState)
            {
                return UserGameState.IN_LOBBY;
            }
            if (userGameRelation.Game.Running)
            {
                return UserGameState.IN_GAME;
            }

            return UserGameState.NOT_IN_GAME;
        }
        public List<string> GetAllPlayersOfGameIds(int gameId, bool lobbyState)
        {
            return _dbContext.userToGame.Include(g => g.Game).Where(utg => utg.GameID == gameId && utg.AcceptedRequest && utg.Active && utg.Game.LobbyState == lobbyState).Select(utg => utg.UserID).ToList();
        } 
        public List<UserToViewInfo> GetAllLobbyInvites(int lobbyId)
        {
            return _dbContext.userToGame.Include(g => g.Game).Include(g => g.User).Where(utg => utg.GameID == lobbyId && utg.Active && utg.Game.LobbyState).Select(utg => new UserToViewInfo() { UserName = utg.User.UserName, InLobby = utg.AcceptedRequest}).ToList();
        }

        public List<GameToViewInfo> GetAllFinishedGamesForUser(string userId)
        {
            List<GameToViewInfo> gtv = new List<GameToViewInfo>();
            var userGames = _dbContext.userToGame.Include(g => g.Game).Where(utg => utg.UserID == userId && utg.Active && utg.AcceptedRequest && utg.Game.LobbyState == false && utg.Game.Running == false).ToList();

            foreach (var userGame in userGames)
            {
                GameToViewInfo gtvInfo = new GameToViewInfo();

                gtvInfo.Id = userGame.GameID;
                gtvInfo.oponents = _dbContext.userToGame.Include(u => u.User).Where(utg => utg.GameID == userGame.GameID && utg.UserID != userId).Select(utg => utg.User.UserName).ToList();
                gtvInfo.Result = userGame.Game.WinnerID == userId ? "WIN" : userGame.Game.WinnerID == null ? "TIE" : "LOSE";

                gtv.Add(gtvInfo);
            }

            return gtv;
        }

        public List<string> GetGameOponents(string userId, int gameId)
        {
            return _dbContext.userToGame.Where(utg => utg.GameID == gameId && utg.UserID != userId).Select(utg => utg.UserID).ToList();
        }

        public string GetGameWinner(int gameId)
        {
            return _dbContext.games.Where(g => g.Id == gameId && g.Running == false && g.LobbyState == false).Select(g => g.WinnerID).FirstOrDefault();
        }

    }
}
