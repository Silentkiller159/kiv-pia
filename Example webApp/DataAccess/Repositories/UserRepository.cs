﻿using BusinessLayer.Models;
using BusinessLayer.Repositories;
using DataAccess.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAccess.Repositories
{
    public class UserRepository : IUserRepository
    {
        private DBContext _dbContext;
        private UserManager<IdentityUser> _userMan;

        public UserRepository(DBContext context, UserManager<IdentityUser> userMan)
        {
            _dbContext = context;
            _userMan = userMan;
        }

        public bool ChangeUserPassword(string userId, string newPass)
        {
            var user = _userMan.FindByIdAsync(userId).Result;
            if (_userMan.RemovePasswordAsync(user).Result.Succeeded)
            {
                return _userMan.AddPasswordAsync(user, newPass).Result.Succeeded;
            }
            return false;
        }

        public bool ChangeUserPasswordValidate(string userId, string newPass, string oldPass)
        {
            var user = _userMan.FindByIdAsync(userId).Result;
            return _userMan.ChangePasswordAsync(user, oldPass, newPass).Result.Succeeded;
        }

        public string GetUserIdByName(string userName)
        {
            return _dbContext.Set<IdentityUser>().Where(u => u.UserName == userName).Select(o => o.Id).FirstOrDefault();
        }

        public string GetUserNameById(string userId)
        {
            return _dbContext.Set<IdentityUser>().Where(u => u.Id == userId).Select(o => o.UserName).FirstOrDefault();
        }

        public List<UserToViewInfo> GetAllUsers()
        {
            return _dbContext.Set<IdentityUserRole<string>>().Select(o => new UserToViewInfo() { 
                UserName = _dbContext.Set<IdentityUser>().Where(u => o.UserId == u.Id).Select(u => u.UserName).FirstOrDefault(),
                Role = _dbContext.Set<IdentityRole>().Where(r => o.RoleId == r.Id).Select(r => r.Name).FirstOrDefault()
            }).ToList();
        }

        public IEnumerable<UserToFriendModel> GetAllFriends(string userId)
        {
            return _dbContext.friendList.Include(o => o.User).Include(o => o.Friend).Where(u => u.UserID == userId || u.FriendID == userId).Select(o => new UserToFriendModel() { UserName = o.User.UserName, FriendName = o.Friend.UserName, Accepted = o.Accepted}).ToList();
        }

        public bool HasFriendRelation(string fromUserId, string toUserId)
        {
            return _dbContext.friendList.Where(flItem => (flItem.UserID == fromUserId && flItem.FriendID == toUserId) || (flItem.FriendID == fromUserId && flItem.UserID == toUserId)).Count() > 0 ? true : false;
        }

        public int SendFriendRequest(string fromUserId, string toUserId)
        {
            _dbContext.friendList.Add(new FriendListEntity() { UserID = fromUserId, FriendID = toUserId });
            _dbContext.SaveChanges();
            return 0;
        }

        public int ProcessFriendRequest(string fromUserId, string toUserId, bool accepted)
        {
            var relationList = _dbContext.friendList.Where(flItem => (flItem.UserID == fromUserId && flItem.FriendID == toUserId) || (flItem.FriendID == fromUserId && flItem.UserID == toUserId)).ToList();
            if(relationList.Count == 1)
            {
                var relation = relationList.FirstOrDefault();
                if (!relation.Accepted)
                {
                    if (accepted)
                    {
                        relation.Accepted = true;
                        _dbContext.SaveChanges();
                        return 0;
                    }
                    else
                    {
                        return RemoveFriend(fromUserId, toUserId);
                    }
                }
                return -2;
            }

            return -1;
        }

        public int RemoveFriend(string fromUserId, string toUserId)
        {
            var relationList = _dbContext.friendList.Where(flItem => (flItem.UserID == fromUserId && flItem.FriendID == toUserId) || (flItem.FriendID == fromUserId && flItem.UserID == toUserId)).ToList();
            if (relationList.Count == 1)
            {
                var relation = relationList.FirstOrDefault();
                _dbContext.friendList.Remove(relation);
                _dbContext.SaveChanges();
                return 0;
            }

            return -1;
        }

        public bool ChangeUserRole(string userId, string newRole)
        {
            var user = _userMan.FindByIdAsync(userId).Result;
            if (_userMan.IsInRoleAsync(user, "ADMIN").Result)
            {
                if (_userMan.GetUsersInRoleAsync("ADMIN").Result.Count < 2)
                {
                    return false; //Last admin standing check!
                }
                if (_userMan.RemoveFromRoleAsync(user, "ADMIN").Result.Succeeded)
                {
                    return _userMan.AddToRoleAsync(user, newRole).Result.Succeeded;
                }
                
            }
            else
            {
                if (_userMan.RemoveFromRoleAsync(user, "USER").Result.Succeeded)
                {
                    return _userMan.AddToRoleAsync(user, newRole).Result.Succeeded;
                }
            }

            return false;
        }
    }
}
