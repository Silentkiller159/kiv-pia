﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccess
{
    public static class SeedDB
    {

        public static void Seed(UserManager<IdentityUser> userMan, RoleManager<IdentityRole> roleMan)
        {
            roleMan.CreateAsync(new IdentityRole("ADMIN")).Wait();
            roleMan.CreateAsync(new IdentityRole("USER")).Wait();

            var adminUser = new IdentityUser("admin@admin.cz");
            adminUser.Email = "admin@admin.cz";
            userMan.CreateAsync(adminUser).Wait();
            userMan.AddPasswordAsync(adminUser, "Kappa_123").Wait();
            userMan.AddToRoleAsync(adminUser, "ADMIN").Wait();
        }
    }
}
