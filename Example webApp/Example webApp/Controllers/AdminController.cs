﻿using BusinessLayer.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Example_webApp.Controllers
{
    [Authorize(Roles = "ADMIN")]
    public class AdminController : Controller
    {
        private readonly ILogger<AdminController> _logger;
        private IUserService _userService;

        public AdminController(ILogger<AdminController> logger, IUserService userServ)
        {
            _logger = logger;
            _userService = userServ;
        }

        public IActionResult Index()
        {
            return View(_userService.GetAllUsers());
        }

        [HttpPost]
        public IActionResult OnPost([FromBody] string data)
        {
            return Ok();
        }
    }
}
