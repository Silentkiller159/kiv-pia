﻿using BusinessLayer.Services;
using Example_webApp.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace Example_webApp.Controllers
{
    public class GameController : Controller
    {

        private readonly ILogger<GameController> _logger;
        private IUserService _userService;

        public GameController(ILogger<GameController> logger, IUserService userServ)
        {
            _logger = logger;
            _userService = userServ;
        }

        [Authorize]
        public IActionResult Index()
        {
            return View("Index", HttpContext.User.Identity.Name);
        }

        [Authorize]
        public IActionResult Lobby()
        {
            return View();
        }

        [Authorize]
        public IActionResult Active()
        {
            return View();
        }

        public IActionResult Replay(int id)
        {
            return View(id);
        }

        [Authorize]
        public IActionResult History()
        {
            return View();
        }
    }
}
