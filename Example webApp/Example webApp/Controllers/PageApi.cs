﻿ using BusinessLayer.Enums;
using BusinessLayer.Models;
using BusinessLayer.Services;
using Example_webApp.WebSocketComm;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Example_webApp.Controllers
{
    [Route("api")]
    [ApiController]
    public class PageApi : ControllerBase
    {
        private readonly ILogger<PageApi> _logger;
        private IUserService _userService;
        private IFriendService _friendService;
        private IGameService _gameService;
        private IHubContext<LobbyHub> _lobbyHub;

        public PageApi(ILogger<PageApi> logger, IUserService userServ, IFriendService friendServ, IGameService gameServ, IHubContext<LobbyHub> lobbyHub)
        {
            _logger = logger;
            _userService = userServ;
            _friendService = friendServ;
            _gameService = gameServ;
            _lobbyHub = lobbyHub;
        }

        // GET: api/<ApiController>
        [HttpGet("GetOnlineUsers")]
        public IEnumerable<string> GetOnlineUsers()
        {
            return NotifyHub.GetOnlineUsers();
        }

        [HttpGet("GetOnlineUsersWithoutFriends")]
        public IEnumerable<UserToViewInfo> GetOnlineUsersWithoutFriends()
        {
            List<UserToViewInfo> onlineUsers = new List<UserToViewInfo>();
            foreach(var user in NotifyHub.GetOnlineUsers())
            {
                if(user != HttpContext.User.Identity.Name)
                {
                    string userId = _userService.GetUserIdByName(user);

                    UserToViewInfo uInfo = new UserToViewInfo();
                    uInfo.UserName = user;
                    UserGameState userState = _gameService.GetUserGameState(userId);
                    if (UserGameState.IN_GAME == userState)
                    {
                        uInfo.InGame = true;
                    }
                    else if (UserGameState.IN_LOBBY == userState)
                    {
                        uInfo.InLobby = true;
                    }
                    onlineUsers.Add(uInfo);
                }
            }

            var myId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            foreach (var user in _friendService.GetAllFriends(myId))
            {
                var userToRemove = user.FriendName == HttpContext.User.Identity.Name ? user.UserName : user.FriendName;
                var userToRemoveObj = onlineUsers.Where( u => u.UserName == userToRemove).FirstOrDefault();
                if (userToRemoveObj != null)
                {
                    onlineUsers.Remove(userToRemoveObj);
                }
            }
            return onlineUsers;
        }

        // GET api/<ApiController>/5
        [HttpGet("GetFriendList")]
        public ActionResult<UserToFriendModel> GetFriendList()
        {
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            if (userId is null)
            {
                return NotFound();
            }

            var friendList = _friendService.GetAllFriends(userId);
            foreach (var user in friendList)
            {
                string friendName = user.FriendName == HttpContext.User.Identity.Name ? user.UserName : user.FriendName;
                string friendId = _userService.GetUserIdByName(friendName);

                if (NotifyHub.connectedUsers.Any(u => u.Item2 == friendName))
                {
                    user.FriendOnline = true;
                }
                UserGameState friendState = _gameService.GetUserGameState(friendId);
                if (UserGameState.IN_GAME == friendState)
                {
                    user.FriendInGame = true;
                }
                else if (UserGameState.IN_LOBBY == friendState)
                {
                    user.FriendInLobby = true;
                }
            }
            return Ok(friendList);
        }

        [HttpPost("SendFriendRequest/{userName}")]
        public IActionResult SendFriendRequest(string userName)
        {
            var myId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            string userId = _userService.GetUserIdByName(userName);
            if (userId is null)
            {
                return NotFound();
            }
            if (_friendService.SendFriendRequest(myId, userId) == 0)
            {
                return Ok();
            }
            else
            {
                return BadRequest();
            }
        }

        [HttpPost("AcceptFriendRequest/{userName}")]
        public IActionResult AcceptFriendRequest(string userName)
        {
            var myId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            string userId = _userService.GetUserIdByName(userName);
            if (userId is null)
            {
                return NotFound();
            }
            if (_friendService.ProcessFriendrequest(myId, userId, true) == 0)
            {
                return Ok();
            }
            else
            {
                return BadRequest();
            }
        }

        [HttpPost("DeclineFriendRequest/{userName}")]
        public IActionResult DeclineFriendRequest(string userName)
        {
            var myId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            string userId = _userService.GetUserIdByName(userName);
            if (userId is null)
            {
                return NotFound();
            }
            if (_friendService.ProcessFriendrequest(myId, userId, false) == 0)
            {
                return Ok();
            }
            else
            {
                return BadRequest();
            }
        }

        [HttpPost("RemoveFriend/{userName}")]
        public IActionResult RemoveFriend(string userName)
        {
            var myId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            string userId = _userService.GetUserIdByName(userName);
            if (userId is null)
            {
                return NotFound();
            }
            if (_friendService.RemoveFriend(myId, userId) == 0)
            {
                return Ok();
            }
            else
            {
                return BadRequest();
            }
        }


        [HttpGet("GetUserGameState")]
        public ActionResult<string> GetUserGameState()
        {
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            if (userId is null)
            {
                return NotFound();
            }

            string state_string = "NOT_IN_GAME";
            UserGameState state = _gameService.GetUserGameState(userId);
            if(state == UserGameState.IN_LOBBY)
            {
                state_string = "IN_LOBBY";
            }
            if (state == UserGameState.IN_GAME)
            {
                state_string = "IN_GAME";
            }
            return Ok(state_string);
        }

        [HttpGet("GetUserGameLobbyId")]
        public ActionResult<string> GetUserGameLobbyId()
        {
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);

            if (userId is null)
            {
                return NotFound();
            }
            int lobbyId = _gameService.GetUserActiveGameLobbyId(userId);

            if (lobbyId > -1)
            {
                return Ok(lobbyId);
            }
            return BadRequest();
        }

        [HttpPost("CreateGameLobby")]
        public IActionResult CreateGameLobby([FromBody] LobbyCreateModel value)
        {
            //[FromBody] string value
            var myId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            int width = -1;
            int height = -1;
            try
            {
                width = int.Parse(value.BoardWidth);
                height = int.Parse(value.BoardHeight);
            }
            catch {
                return BadRequest();
            }
            if (width >= 5 && height >= 5)
            {
                int returnCode = _gameService.CreateGameLobby(myId, width, height);
                if (returnCode > -1)
                {
                    return Ok();
                }
                else
                {
                    return BadRequest();
                }
            }
            else
            {
                return BadRequest();
            }
        }

        [HttpPost("InviteToGameLobby/{userName}")]
        public IActionResult InviteToGameLobby(string userName)
        {
            var myId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            string userId = _userService.GetUserIdByName(userName);


            if (_gameService.InviteToGameLobby(userId, myId) > -1)
            {
                return Ok();
            }
            else
            {
                return BadRequest();
            }
        }

        [HttpGet("GetInvitesList")]
        public ActionResult<IEnumerable<int>> GetInvitesList()
        {
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            if (userId is null)
            {
                return NotFound();
            }
            return Ok(_gameService.GetUserInvites(userId));
        }

        [HttpGet("GetMyLobbyPlayers")]
        public ActionResult<List<UserToViewInfo>> GetMyLobbyPlayers()
        {
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);

            if (userId is null)
            {
                return NotFound();
            }
            int lobbyId = _gameService.GetUserActiveGameLobbyId(userId);

            if (lobbyId > -1)
            {
                return Ok(_gameService.GetGameInvites(lobbyId));
            }
            return BadRequest();
        }

        [HttpPost("JoinGameLobby/{lobbyId}")]
        public IActionResult JoinGameLobby(int lobbyId)
        {
            var myId = User.FindFirstValue(ClaimTypes.NameIdentifier);

            if (_gameService.ProcessGameInviteRequest(myId, lobbyId, true))
            {
                return Ok();
            }
            else
            {
                return BadRequest();
            }
        }

        [HttpPost("DeclineGameRequest/{lobbyId}")]
        public IActionResult DeclineGameRequest(int lobbyId)
        {
            var myId = User.FindFirstValue(ClaimTypes.NameIdentifier);

            if (_gameService.ProcessGameInviteRequest(myId, lobbyId, false))
            {
                var listOfPlayers = _gameService.GetAllPlayersOfLobbyIds(lobbyId);
                foreach (var player in listOfPlayers)
                {
                    _lobbyHub.Clients.User(player).SendAsync("AcceptLobbyMessage", _userService.GetUserNameById(myId) + " declined!");
                }
                return Ok();
            }
            else
            {
                return BadRequest();
            }
        }

        [HttpPost("LeaveGameLobby")]
        public IActionResult LeaveGameLobby()
        {
            var myId = User.FindFirstValue(ClaimTypes.NameIdentifier);

            if (_gameService.LeaveGameLobby(myId))
            {
                return Ok();
            }
            else
            {
                return BadRequest();
            }
        }

        [HttpGet("GetMyGameHistory")]
        public ActionResult<List<GameToViewInfo>> GetMyGameHistory()
        {
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);

            if (userId is null)
            {
                return NotFound();
            }
            var listGames = _gameService.GetAllFinishedGamesForUser(userId);
            listGames.Reverse();
            return Ok(listGames);
        }

        [HttpGet("GetGameHistoryView/{id}")]
        public ActionResult<GameHistoryToView> GetGameHistoryView(int id)
        {
            var history = _gameService.GetFinishedGameHistory(id);
            history.GameOwner = _userService.GetUserNameById(history.GameOwner);
            history.Player2 = _userService.GetUserNameById(history.Player2);
            history.Winner = _userService.GetUserNameById(history.Winner);
            return Ok(history);
        }

        [HttpPost("PasswordChange")]
        public IActionResult PasswordChange([FromBody] PasswordChangeModel model)
        {
            var myId = User.FindFirstValue(ClaimTypes.NameIdentifier);

            if (_userService.ChangePasswordValidate(myId, model.Password, model.OldPassword))
            {
                return Ok();
            }
            else
            {
                return BadRequest();
            }
        }

        [Authorize(Roles = "ADMIN")]
        [HttpPost("Admin/RoleChange")]
        public IActionResult AdminRoleChange([FromBody] RoleChangeModel model)
        {
            var userId = _userService.GetUserIdByName(model.UserName);

            if (userId != null)
            {
                if(_userService.ChangeUserRole(userId, model.NewRole))
                {
                    return Ok();
                }
            }

            return BadRequest();
        }
        
        [Authorize(Roles = "ADMIN")]
        [HttpPost("Admin/PasswordChange")]
        public IActionResult AdminPasswordChange([FromBody] PasswordChangeModel model)
        {
            var myId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            var userId = _userService.GetUserIdByName(model.UserName);

            if (userId != null && myId != userId)
            {
                if(_userService.ChangePassword(userId, model.Password))
                {
                    return Ok();
                }
            }

            return BadRequest();
        }

    }
}
