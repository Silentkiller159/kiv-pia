﻿using BusinessLayer.Repositories;
using BusinessLayer.Services;
using DataAccess.Repositories;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Example_webApp.Services
{
    public static class ServiceExtension
    {
        public static void AddServices(this IServiceCollection servColl)
        {
            servColl.AddTransient<IUserService, UserService>();
            servColl.AddTransient<IUserRepository, UserRepository>();
            servColl.AddTransient<IGameService, GameService>();
            servColl.AddTransient<IFriendService, FriendService>();
            servColl.AddTransient<IGameRepository, GameRepository>();

        }
    }
}
