﻿using BusinessLayer.Models;
using BusinessLayer.Services;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Example_webApp.WebSocketComm
{
    /// <summary>
    /// This class is for handling communication Server<-> Players
    /// Controlls game
    /// </summary>
    public class GameHub : Hub
    {
        private ILogger<GameHub> _logger;

        private IUserService _userService;
        private IGameService _gameService;

        public GameHub(ILogger<GameHub> logger, IUserService userServ, IGameService gameServ)
        {
            _logger = logger;
            _userService = userServ;
            _gameService = gameServ;
        }

        public override Task OnConnectedAsync()
        {
            _logger.LogInformation($"Game Hub user connected: {Context.User.Identity.Name}");
            InitGame();
            return base.OnConnectedAsync();
        }

        public override Task OnDisconnectedAsync(Exception exception)
        {
            _logger.LogInformation($"Game Hub user disconnected: {Context.User.Identity.Name}");
            return base.OnDisconnectedAsync(exception);
        }

        private async Task InitGame()
        {
            string userId = _userService.GetUserIdByName(Context.User.Identity.Name);
            int gameId = _gameService.GetUserActiveGameId(userId);
            GameSizeModel gsm = _gameService.GetCurrentGameSize(userId);
            if (gsm != null && gameId > -1)
            {
                var listOfPlayers = _gameService.GetAllPlayersOfGameIds(gameId);
                string ownerId = _gameService.GetCurrentGameOwnerId(gameId);
                string player2 = listOfPlayers[0] == ownerId ? listOfPlayers[1] : listOfPlayers[0];
                await Clients.User(userId).SendAsync("Drawtable", gsm.Width, gsm.Height, _userService.GetUserNameById(ownerId), _userService.GetUserNameById(player2));
                await Clients.User(userId).SendAsync("ChangeCurrentPlayer", _userService.GetUserNameById(_gameService.GetCurrentGamePlayerId(gameId)));

                GameMoveViewModel[,] gameBoard = GetBoardState(gameId, gsm.Width, gsm.Height);
                List<string> listImageState = new List<string>();
                for (int i = 0; i < gsm.Height; i++)
                {
                    for (int j = 0; j < gsm.Width; j++)
                    {
                        listImageState.Add(gameBoard[i, j].ImgPath);
                    }
                }

                await Clients.User(userId).SendAsync("UpdateGameTable", listImageState);
            }
        }

        private GameMoveViewModel[,] GetBoardState(int gameId, int width, int height)
        {
            var changes = _gameService.GetGameHistory(gameId);

            GameMoveViewModel[,] boardArr = new GameMoveViewModel[height, width];
            for(int i = 0; i < height; i++)
            {
                for(int j = 0; j < width; j++)
                {
                    boardArr[i, j] = new GameMoveViewModel() { PosX = j, PosY = i, ImgPath = "/resources/white.png", TimeStamp = new DateTime() };
                }
            }

            var firstPlayerName = "";
            var firstPlayer = changes.FirstOrDefault();
            if(firstPlayer != null)
            {
                firstPlayerName = firstPlayer.OwnerName;
            }
            foreach(var change in changes)
            {
                var item = boardArr[change.PosY, change.PosX];
                item.TimeStamp = change.TimeStamp;
                item.ImgPath = firstPlayerName != change.OwnerName ? "/resources/circle.svg" : "/resources/cross_x.svg";
            }

            return boardArr;
        }

        public async Task SendGameAction(int x, int y)
        {
            string userId = _userService.GetUserIdByName(Context.User.Identity.Name);
            int gameId = _gameService.GetUserActiveGameId(userId);
            GameSizeModel gsm = _gameService.GetCurrentGameSize(userId);
            if (gsm != null || gameId < 0)
            {
                if (_gameService.AcceptGameMove(userId, x, y))
                {
                    bool gameEndsTie = false;
                    int positionCount = gsm.Width * gsm.Height;                

                    GameMoveViewModel[,] gameBoard = GetBoardState(gameId, gsm.Width, gsm.Height);
                    string gameWinnerId = AnalyzeWinner(gameBoard, x, y, gsm.Width, gsm.Height) ? userId : null;
                    List<string> listImageState = new List<string>();

                    for (int i = 0; i < gsm.Height; i++)
                    {
                        for (int j = 0; j < gsm.Width; j++)
                        {
                            listImageState.Add(gameBoard[i, j].ImgPath);
                        }
                    }

                    if(gameWinnerId != null)
                    {
                        _gameService.EndGame(gameId, gameWinnerId);
                    }
                    else if (_gameService.GetGameHistory(gameId).Count >= positionCount)
                    {
                        _gameService.EndGame(gameId, null);
                        gameEndsTie = true;
                    }

                    var listOfPlayers = _gameService.GetAllPlayersOfGameIds(gameId);
                    foreach (var player in listOfPlayers)
                    {
                        await Clients.User(player).SendAsync("UpdateGameTable", listImageState);
                        await Clients.User(player).SendAsync("ChangeCurrentPlayer", _userService.GetUserNameById(_gameService.GetCurrentGamePlayerId(gameId)));

                        if(gameWinnerId != null)
                        {
                            await Clients.User(player).SendAsync("ProcessWinner", _userService.GetUserNameById(gameWinnerId));
                        }
                        else if (gameEndsTie)
                        {
                            await Clients.User(player).SendAsync("ProcessTie");
                        }
                    }
                }
            }
        }

        private bool AnalyzeWinner(GameMoveViewModel[,] gameBoard, int posX, int posY, int width, int height)
        {
            string imgType = gameBoard[posY, posX].ImgPath;

            int winLimit = 5;

            //Check states Vertical, Horizontal and X

            int xLeft = 0;
            int xRight = 0;

            for(int i = 1; true ; i++)
            {
                //Check X right
                if (imgType == GetPositionData(gameBoard, posX + i, posY, width, height))
                {
                    xRight++;
                }
                else
                {
                    break;
                }
            }

            for (int i = 1; true; i++)
            {
                //Check X left
                if (imgType == GetPositionData(gameBoard, posX - i, posY, width, height))
                {
                    xLeft++;
                }
                else
                {
                    break;
                }
            }

            if (xRight + xLeft + 1 >= winLimit)
            {
                return true;
            }

            int yUp = 0;
            int yDown = 0;

            for (int i = 1; true; i++)
            {
                //Check Y up
                if (imgType == GetPositionData(gameBoard, posX, posY - i, width, height))
                {
                    yUp++;
                }
                else
                {
                    break;
                }
            }

            for (int i = 1; true; i++)
            {
                //Check Y down
                if (imgType == GetPositionData(gameBoard, posX, posY + i, width, height))
                {
                    yDown++;
                }
                else
                {
                    break;
                }
            }

            if (yDown + yUp + 1 >= winLimit)
            {
                return true;
            }

            int forwardSlashUp = 0;
            int forwardSlashDown = 0;

            for (int i = 1; true; i++)
            {
                //Check forward Slash UP
                if (imgType == GetPositionData(gameBoard, posX + i, posY - i, width, height))
                {
                    forwardSlashUp++;
                }
                else
                {
                    break;
                }
            }

            for (int i = 1; true; i++)
            {
                //Check forward Slash down
                if (imgType == GetPositionData(gameBoard, posX - i, posY + i, width, height))
                {
                    forwardSlashDown++;
                }
                else
                {
                    break;
                }
            }

            if (forwardSlashUp + forwardSlashDown + 1 >= winLimit)
            {
                return true;
            }

            int backSlashUp = 0;
            int backSlashDown = 0;

            for (int i = 1; true; i++)
            {
                //Check back Slash UP
                if (imgType == GetPositionData(gameBoard, posX - i, posY - i, width, height))
                {
                    backSlashUp++;
                }
                else
                {
                    break;
                }
            }

            for (int i = 1; true; i++)
            {
                //Check back Slash down
                if (imgType == GetPositionData(gameBoard, posX + i, posY + i, width, height))
                {
                    backSlashDown++;
                }
                else
                {
                    break;
                }
            }

            if (backSlashUp+backSlashDown+1 >= winLimit)
            {
                return true;
            }

            
            return false;
        }

        private string GetPositionData(GameMoveViewModel[,] gameBoard, int posX, int posY, int width, int height)
        {
            if (posX >= 0 && posY >= 0 && posX < width && posY < height)
            {
                return gameBoard[posY, posX].ImgPath;
            }
            return null;
        }

        public async Task SendMessage(string message)
        {
            string userId = _userService.GetUserIdByName(Context.User.Identity.Name);
            int gameId = _gameService.GetUserActiveGameId(userId);
            if (gameId > -1)
            {
                var listOfPlayers = _gameService.GetAllPlayersOfGameIds(gameId);
                foreach (var player in listOfPlayers)
                {
                    await Clients.User(player).SendAsync("ReceiveMessage", Context.User.Identity.Name, message);
                }
            }
        }

        public async Task Surrender()
        {
            string userId = _userService.GetUserIdByName(Context.User.Identity.Name);
            int gameId = _gameService.GetUserActiveGameId(userId);
            if (gameId > -1)
            {       
                var listOfPlayers = _gameService.GetAllPlayersOfGameIds(gameId);
                string player2 = listOfPlayers[0] == userId ? listOfPlayers[1] : listOfPlayers[0];
                _gameService.EndGame(gameId, player2);
                foreach (var player in listOfPlayers)
                {
                    await Clients.User(player).SendAsync("ProcessWinner", _userService.GetUserNameById(player2));
                }
            }
        }
    }
}
