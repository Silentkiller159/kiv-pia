﻿using BusinessLayer.Services;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Example_webApp.WebSocketComm
{
    public class LobbyHub : Hub
    {
        private ILogger<LobbyHub> _logger;

        private IUserService _userService;
        private IGameService _gameService;

        public LobbyHub(ILogger<LobbyHub> logger, IUserService userServ, IGameService gameServ)
        {
            _logger = logger;
            _userService = userServ;
            _gameService = gameServ;
        }

        public override Task OnConnectedAsync()
        {
            SendMessageToLobby(" joined the lobby!");
            return base.OnConnectedAsync();
        }

        public override Task OnDisconnectedAsync(Exception exception)
        {
            SendMessageToLobby(" left the lobby!");
            return base.OnDisconnectedAsync(exception);
        }

        public async Task SendMessageToLobby(string message)
        {
            var listOfPlayers = _gameService.GetAllPlayersOfLobbyIds(_gameService.GetUserActiveGameLobbyId(_userService.GetUserIdByName(Context.User.Identity.Name)));
            foreach(var player in listOfPlayers)
            {
                await Clients.User(player).SendAsync("AcceptLobbyMessage", message, Context.User.Identity.Name);
            }
        }

        public async Task StartGame()
        {
            var userId = _userService.GetUserIdByName(Context.User.Identity.Name);
            var listOfPlayers = _gameService.GetAllPlayersOfLobbyIds(_gameService.GetUserActiveGameLobbyId(userId));
            if (_gameService.StartCurrentGame(_userService.GetUserIdByName(Context.User.Identity.Name)))
            {
                //Game started correctly
                foreach (var player in listOfPlayers)
                {
                    await Clients.User(player).SendAsync("StartGame");
                }
            }
            else
            {
                await Clients.User(userId).SendAsync("AcceptLobbyMessage", "Could not start the game!");
            }
        }

        /*
        public async Task StartGame()
        {
         //   await Clients.User(_userService.GetUserIdByName(user)).SendAsync("AcceptNotification", message);
        }*/
    }
}
