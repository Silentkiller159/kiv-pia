﻿using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Example_webApp.WebSocketComm
{
    /// <summary>
    /// This class is for chat communication
    /// 
    /// </summary>
    public class MessageHub : Hub
    {

        private ILogger<MessageHub> logger;

        public MessageHub(ILogger<MessageHub> logger)
        {
            this.logger = logger;
        }

        public override Task OnConnectedAsync()
        {
            logger.LogInformation($"Message Hub user connected: {Context.User.Identity.Name}");
            return base.OnConnectedAsync();
        }

        public override Task OnDisconnectedAsync(Exception exception)
        {
            logger.LogInformation($"Message Hub user disconnected: {Context.User.Identity.Name}");
            return base.OnDisconnectedAsync(exception);
        }

        public async Task SendMessage(string user, string message)
        {
            await Clients.All.SendAsync("ReceiveMessage", Context.User.Identity.Name, message);

            //await Clients.User(user).SendAsync("ReceiveMessage", Context.User.Identity.Name, message);
        
        }
    }
}
