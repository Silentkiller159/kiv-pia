﻿using BusinessLayer.Repositories;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Example_webApp.WebSocketComm
{
    public class NotifyHub : Hub
    {

        private ILogger<NotifyHub> _logger;
        private IUserRepository _userRepo;

        /// <summary>
        /// (string ,string) = connectionID, userMail
        /// </summary>
        public static List<(string, string)> connectedUsers = new List<(string, string)>();

        public NotifyHub(ILogger<NotifyHub> logger, IUserRepository userRepo)
        {
            this._logger = logger;
            this._userRepo = userRepo;
        }

        public override Task OnConnectedAsync()
        {
            connectedUsers.Add((Context.ConnectionId, Context.User.Identity.Name));
            //Clients.All.SendAsync("UserUpdate", GetOnlineUsers());
            return base.OnConnectedAsync();
        }

        public override Task OnDisconnectedAsync(Exception exception)
        {
            //Clients.All.SendAsync("UserUpdate", GetOnlineUsers());
            connectedUsers.Remove((Context.ConnectionId, Context.User.Identity.Name));
            return base.OnDisconnectedAsync(exception);
        }

        public async Task NotifyUser(string user, string message)
        {
            _logger.LogInformation("User notify: " + user);
            await Clients.User(_userRepo.GetUserIdByName(user)).SendAsync("AcceptNotification", message);

        }

        public static List<string> GetOnlineUsers()
        {
            return connectedUsers.Where(t => t.Item2 != null).Select(t => t.Item2).Distinct().ToList();
        }
    }
}
