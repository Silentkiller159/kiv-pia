Aplikace je díky docker-compose velmi snadná na spuštění.
Sama se při prvním spuštění nastaví, včetně databáze.

Pro její spuštění je třeba mít nainstalovaný Docker.

Spuštění
- V adresáří projektu, kde je soubor docker-compose.yml spustit příkaz: docker-compose up --build
- Po zavolání přikazu se stáhne vše potřebné pro spuštění MySQL a ASP.NET Core Aplikace
- Dojde k sestavení aplikace
- Po spuštění je aplikace dostupná na "http://localhost:5000"
- Pro přihlášení je potřeba se registrovat
- ADMIN má login
  - MAIL: admin@admin.cz
  - PASSWORD: Kappa_123

Troubleshooting
- Při prvotním spuštění (se stažením databáze) se 1x ze 3 případů stalo, že databáze se nestihla spustit správně a aplikace k ní již chtěla přistoupit.
  Řešením bylo prostě zavolat "docker-compose up" znovu, nyní již bylo vše v pořádku a aplikace bylo spuštěna. 
