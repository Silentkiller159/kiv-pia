# Úvod
Aplikace je postavena v ASP.NET Core spolu s MySQL. Důvod, proč není v preferované Javě je jednoduchý. V práci nás čeká projekt v těchto technologíích a byl jsem požádán abych se na něm podílel, moje zkušenosti s vývojem webů, mimo PHP, jsou prakticky nulové a toto vypadá jako skvělá příležitost jak to změnit.

# Použité technologie
- ASP.NET Core 3.1
    - Entity Framework
- MySQL 5.7
- Swagger
- HTML + CSS
    - [Bootstrap](https://getbootstrap.com/)
- REST
    - GET / POST
- Web sockets
    - [SignalR](https://dotnet.microsoft.com/apps/aspnet/signalr)
- Docker
- Docker compose

# Architektura
## Databáze
Databáze je typu MySQL a má následující tabulky pro korektní práci se hrou.

### Tabulka pro Hry
Obsahuje
- ID
    - Identifikátor hry
- Width
    - Šířka
- Height
    - Výška
- Current Player
    - ID Aktuálního hráče, ze začátku stejné jako Owner
- Running
    - Příznak, jestli hra aktuálně probíhá
- Winner
    - ID Hráče který hru vyhrál, v případě remízy je null
- Lobby State
    - Příznak, jestli je hra ve fázi lobby
- Owner
    - ID uživatele který je vlastníkem (Zakladatelem) hry

### Tabulka pro Tahy
Obsahuje
- ID
    - Identifikátor tahu
- Game ID
    - ID Hry do které tah náleží
- Pos X
    - X souřadnice tahu
- Pos Y
    - Y souřadnice tahu
- Owner
    - ID Hráče který políčko vlastní
- Timestamp
    - čas zabrání políčka

### Tabulka pro vazbu mezi hráčem a hrou
Obsahuje
- ID
    - Identifikátor vazby
- Game ID
    - ID Hry 
- User ID
    - ID uživatele 
- Game ID
    - ID Hry 
- Active
    - příznak aktivní hry
- Accepted Request
    - Příznak, jestli uživatel přijal pozvánku do hry

### Tabulka pro přátele
Vazba přátel je oboustraná, pokud je uživatel A přítelem B, pak je i B přítelem A. Netřeba další záznam.
Obsahuje
- ID
    - Identifikátor vazby
- User ID
    - ID uživatele
- Friend ID
    - ID přítele 
- Accepted
    - Příznak, jestli uživatel přijal pozvánku a jsou přáteli

## Aplikace
Řešení stojí na existující databázi, která je zdrojem dat, tato data řídí, co je v aplikaci aktuálně možno provádět a co ne.

Pro práci s databází je zde projekt DataAccess, ten obsahuje entity, které jsou v databázi a tak třídy GameRepository a UserRepository, které zprostředkovávají funkce pro práci s databází. Samotná struktura databáze je definována třídou DBContext.

Pro práci s logikou a jako mezivrstva je zde projekt BusinessLayer, ten zprostředkovává služby pro řízení hry, či uživatelů a vnitřně volá funkce Repozitářů, definovaných v rozhraních, tudíž není závislý na DataAccess.
Služby jsou také implementovány za pomoci rozhraní a opět je možné vytvořit jiné implementace při dodržení podmínek rozhraní.
Mezivrstva obsahuje modely dat, které je potřeba předávat zbytku řešení. 

Hlavní projekt obsahuje kontrolery pro řízení obsahu. Je zde i třída PageApi, které obstarává všechny funkce API pro aplikaci. Nachází se zde i "Huby" pro Web Socket komunikaci, které obsatarávají volání/naslouchání na socketech a řídí odpovědi. V neposlední řade se zde nachází všechny pohledy na aplikaci. 

## Logika
Aplikace se řídí několika pravidly, které jsou většinou na první pohled jasné z toho, co aplikace vůbec nabízí při pohledu z webového prohlížeče.

Zde jen stručně ty, co nemusí být na první pohled zřejmé
- Uživatel si může změnit vlastní heslo, pokud zná to staré, jinak to nejde (Musí přes admina)
- Admin může měnit heslo bez znalosti starého všem (Uživatelům i jiným adminům) bez znalosti starého, ale sám sobě musí v User panelu a se znalostí starého hesla
- Do lobby je možné pozvat lidi bez omezení, ale v případě připojení 2 lidí do lobby je připojení zamítnuto.
- Pozvánky do hry které již začla a nebyly přijaty jsou smazány při startu hry
- Hra v prohlížeči odesílá pouze požadavek o zabrání políčka, to jestli je potvrzen či zamítnut vyhodnocuje back-end, který v případě potvrzení zabrání políčka informuje všechny uživatele o změně a provede takžetéž změnu aktuálního hráče a opět informuje všechny zainteresované.
- Hra má neomezený board, takže vyhodnocený výhry je pomocí optimalizovaného BFS alogirtmu (Z políčka kliku hledám jestli je pro aktuálního uživatele zabráno 5, či více políček)
- Hra vyhodnotí plný board, ale neumí rozpoznat jestli ještě před naplněním je možná výhra.
- Replay hry probíhá nahráním všech tahů a vykreslením v fixním intervalu v prohlížeči (Původní myšlenka přehrávat se stejným čase se projevila jako nevhodná)

# Implementované vlastnosti
## Systém přátel
Systém online uživatelů a přátel je navržen trochu nehezky, ale šlo o ukázku práce s API, stránka se cyklicky dotazuje 1x za 5 vteřin na stav uživatelů. Zde nejsou aplikovány web sockety. Všechny požadavky nad uživately jsou pomocí GET/POST dotazů. 

V záložce Game, možnost přidat přítele, pomocí "Send friend request".
![friend request](./img/sendfriendrequest.png)

Odeslané žádosti se zobrazují jako "OUTGOING" a je možné je zrušit.
![User panel](./img/outgoingfriend.png)

Příchozí žádosti o přátelství je možné přijmout, či odmítnout.
![User panel](./img/incomingfriend.png)

Přátele je možné odebrat.
![User panel](./img/removefriend.png)

U přátel jsou vidět stavy: 

![online friend](./img/onlinefriend.png)
![offline friend](./img/offlinefriend.png)
![in lobby friend](./img/lobbyfriend.png)
![in game friend](./img/gamefriend.png)

## Lobby
Lobby je možné vytvořit s "neomezenou" velikostí. Minimum je 5x5 políček. Při extrémních velikostech je zde limit zobrazení ze strany prohlížeče. Původně myšlenka byla dovolit více hráčů, ale nakonec jsem lobby osekal na max 2 hráče, ale s možností pozvat další, kdyby někdo nepřijal aby byla náhrada. Pod lobby je vidět stav hráčů: Připojen/Pozván.

Hru zahajuje zakladatel lobby, než se napojí lidi je zde k dispozici chat + stavové zprávy o lobby. Typicky: Připojen/odpojen a pozván/odmítnuto.

## Hra
Hra začíná vždy zakladatelem lobby a pak se střídají tahy. Zobrazuje se aktuálně hrající uživatel.

Hra vyhodnocuje výhru po každém tahu a pokud někdo vyhrál zobrazí informaci o výhře, v případě že je herní plocha plná, hra bude ukončena remízou.

Hru je možné vzdát pomocí tlačítka vpravo nahoře.

### Chat
Pod tabulkou se hrou je k dispozici in-game chat. Zprávy jsou ve stejném formátu jako Global Chat.

## Historie
Historie zobrazuje počet vyhraných/remizovaných/prohraných her, přehledně v pravo nahoře.
Jinak jsou zde bloky her seřazeny od nejnovější s informací o výhře a oponenetech.

### Přehrání hry
V historii her je možné si přehrát proběhlé hry, stačí kliknout a dojde k přehrávání.

## Uživatelský panel
Možnost změny hesla přihlášeného uživatele.
![User panel](./img/userpanel.png)

## Swagger
Velice užitečná funkcionalita, pomohla při debugu. Je dostupný na adrese "/Swagger".
![Swagger](./img/swagger.png)

# Funkcionality navíc
## Gravatary
Celá aplikace podporuje tvz. [Gravatary](https://cs.gravatar.com/). Jedná se o přiřazení profilového obrázku pomocí globální databáze, rozpoznání je pomocí e-mailu.
![Gravatar img](./img/gravatar.png)

## Global Chat
Je zde k dispozici globální chat pro všechny přihlášené uživatele.
Podporuje i příkazy, zatím jen "/shrug".
![Global chat](./img/globalchat.png)

# Nedodělané funkcionality
V práci jsem začal některé funkce, navíc, které jsem nestihl dodělat. Snažil jsem se je odpojit od pohledu uživatele. V kódu budou kvůli tomu bohužel možná místy fragmenty, které tomu budou napovídat.

Hlavně se jedná o:
- Ověření registrace na e-mail
- Reset hesla pomocí e-mailu
- OAuth 2 implementace místo klasické registrace

Výše zmíněné jsem bohužel nestihl, zrovna ten OAuth 2 mě velmi mrzí, to jsem si chtěl zkusit.

# Stručný seznam bonusových úkolů
Bonusové úkoly
- Unlimited Board
- Password strength evaluation
- in game-chat
- save games and allow replay
- Swagger (Na adrese "/Swagger")
- (Global chat)
- (Gravatary)
- (Změna hesla uživatele v user panelu)

# Závěr
Osobně nejsem webař a vývoj webů mě vcelku děsil, je to spoustu práce na mnoha rozdílných věcech, od HTML+CSS až po hostování funkčních řešení, loadbalancing atd. Tato práce mě naučila s těmito technologiemi vcelku pracovat, určitě jedna zkušenost neznamená že se tím dá živit, ale je to dobrý začátek. Velice mě potěšilo, že aplikace funguje, dokonce i s funkcionalitami navíc, jako je třeba "replay", který se mi osobně velice líbí. Díky práci jsem se celkem zorientoval v ASP.NETu a to považuji za největší přínos z celé této práce.
